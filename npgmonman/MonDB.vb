﻿Public Class MonDB

    ''' <summary>
    ''' The list of mobs in the DB
    ''' </summary>
    ''' <returns></returns>
    Private ReadOnly Property Mobs As List(Of Mon)

    ''' <summary>
    ''' The list of names of mobs, used primarily for error checking.
    ''' </summary>
    ''' <returns></returns>
    Private ReadOnly Property MobNames As List(Of String)

    ''' <summary>
    ''' New DB of mobs
    ''' </summary>
    ''' <param name="txt">File containing all Mob Data</param>
    Public Sub New(txt As String)
        Dim nMob As Mon

        Mobs = New List(Of Mon)
        MobNames = New List(Of String)

        For Each l As String In IO.File.ReadAllLines(txt)
            If l.Contains("[MOB]") Then
                nMob = MakeMon(l)
                Mobs.Add(nMob)
                MobNames.Add(nMob.Name)
            End If
        Next
    End Sub

    ''' <summary>
    ''' Make a new Monster
    ''' </summary>
    ''' <param name="ln">Format should be "[MOB]Name:BaseHP:BaseMinDMG:BaseMaxDMG" E.g. "[MOB]Meow:50:10:15"</param>
    ''' <returns></returns>
    Private Function MakeMon(ln As String) As Mon
        Dim nMon As Mon

        nMon = New Mon(ln)

        Return nMon
    End Function

    ''' <summary>
    ''' Gets a monster by it's name
    ''' </summary>
    ''' <param name="Name">It's name as a string, E.g. "Meow"</param>
    ''' <returns>Returns a Mon</returns>
    Private Function MonByName(Name As String) As Mon
        Dim Result As New Mon()

        For Each m As Mon In Mobs
            If m.Name = Name Then
                Result = m
            End If
        Next

        Return Result
    End Function


    ''' <summary>
    ''' Gets a monster by it's location in the list of mobs
    ''' </summary>
    ''' <param name="Index">Integer representing it's index in the list</param>
    ''' <returns>Returns a Mon</returns>
    Private Function MonByID(Index As Integer) As Mon
        Dim Result As Mon

        Result = Mobs(Index)

        Return Result
    End Function

    ''' <summary>
    ''' Gets a random monster from the list of mobs
    ''' </summary>
    ''' <returns>Returns a Mon</returns>
    Private Function RandomMon() As Mon
        Dim Result As Mon

        Result = Mobs(RanGen.Next(0, Mobs.Count - 1))

        Return Result
    End Function

    ''' <summary>
    ''' Gets an instance of a mob
    ''' </summary>
    ''' <param name="MMon">The Monster to base the instance off of</param>
    ''' <param name="Sts">And Status Modifier</param>
    ''' <returns>Returns a MonInst</returns>
    Private Function GetMonSt(MMon As Mon, Sts As Status) As MonInst
        Dim Result As MonInst = New MonInst(MMon)

        Select Case Sts
            Case Status.HPP
                Result.HStatus(HStat.Vibrant)
            Case Status.HPM
                Result.HStatus(HStat.Sickly)
            Case Status.HPR
                Result.HStatus(HStat.Random)
            Case Status.STP
                Result.SStatus(SStat.Strong)
            Case Status.STM
                Result.SStatus(SStat.Weak)
            Case Status.STR
                Result.SStatus(SStat.Random)
            Case Status.RAN
                If RanGen.Next(100000) < 50000 Then
                    Result.HStatus(HStat.Random)
                Else
                    Result.SStatus(SStat.Random)
                End If
        End Select

        Return Result
    End Function

    ''' <summary>
    ''' Gets an instance of a monster based on it's name, can set status here
    ''' </summary>
    ''' <param name="Name">The Name of a monster, E.g. "Meow"</param>
    ''' <param name="Sts">Optionaly give it a status</param>
    ''' <returns>Returns a MonInst</returns>
    Public Function GetMon(Name As String, Optional Sts As Status = Status.NRM) As MonInst
        Dim Result As MonInst = New MonInst(New Mon())

        If MobNames.Contains(Name) Then
            Result = GetMonSt(MonByName(Name), Sts)
        End If

        Return Result
    End Function

    ''' <summary>
    ''' Gets an instance of a monster based on it's name, can set status here
    ''' </summary>
    ''' <param name="ID">The Index of a monster in the list of monsters</param>
    ''' <param name="Sts">Optionaly give it a status</param>
    ''' <returns>Returns a MonInst</returns>
    Public Function GetMon(ID As Integer, Optional Sts As Status = Status.NRM) As MonInst
        Dim Result As MonInst = New MonInst(New Mon())

        If Mobs.Count > 0 Then
            If Mobs.Count <= ID Then
                Result = GetMonSt(MonByID(ID), Sts)
            End If
        End If

        Return Result
    End Function

    ''' <summary>
    ''' Gets a random instance of a monster, can set status here
    ''' </summary>
    ''' <param name="Sts">Optionaly give it a status</param>
    ''' <returns>Returns a MonInst</returns>
    Public Function GetMon(Optional Sts As Status = Status.NRM) As MonInst
        Return GetMonSt(RandomMon, Sts)
    End Function


    ''' <summary>
    ''' Get's how many mobs are currently in the list
    ''' </summary>
    ''' <returns></returns>
    Public Function Count() As Integer
        Return Mobs.Count
    End Function

End Class