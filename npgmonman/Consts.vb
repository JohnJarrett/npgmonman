﻿Public Module Consts

    ''' <summary>
    ''' HP status effects
    ''' </summary>
    Public Enum HStat
        Sickly
        Normal
        Vibrant
        Random
        Read
    End Enum

    ''' <summary>
    ''' DMG status effects 
    ''' </summary>
    Public Enum SStat
        Weak
        Normal
        Strong
        Random
        Read
    End Enum

    Public Enum Status
        ''' <summary>
        ''' Normal
        ''' </summary>
        NRM
        ''' <summary>
        ''' HP Plus
        ''' </summary>
        HPP
        ''' <summary>
        ''' HP Minus
        ''' </summary>
        HPM
        ''' <summary>
        ''' HP Random
        ''' </summary>
        HPR
        ''' <summary>
        ''' STR Plus
        ''' </summary>
        STP
        ''' <summary>
        ''' STR Minus
        ''' </summary>
        STM
        ''' <summary>
        ''' STR Random
        ''' </summary>
        STR
        ''' <summary>
        ''' Random
        ''' </summary>
        RAN
    End Enum

    Public MobDB As MonDB
    Public RanGen As Random

    Public Const HBonus As Single = 0.05 'Health  Mod Bonus
    Public Const SBonus As Single = 0.05 'Strength Mod Bonus
    Public Const LBonus As Single = 0.1 'Ammount to add to prev on level up
    Public Const MaxLevel As Integer = 35 'Max level for pumping out a mob table
    Public Const MobDBFile As String = "Mob.dat" 'The input data file
    Public Const MobTableDir As String = "MobTables" 'the output directory of a mob table
    Public Const MobTableFileExt As String = "txt" 'What to shove on the end of the individual mob table output files

    Public Const RanStsMin As Integer = 5000 'When determining a random status for Health or Strength, what would be weak/sickly
    Public Const RanStsMax As Integer = 35000 'When determining a random status for Health or Strength, what would be Vibrant/Strong
    Public Const RanStsNum As Integer = 40000 'When determining a random status for Health or Strength, What should our max gnerated number be

    ' At what level should we apply the level bonus
    Public Const LvlA As Integer = 0
    Public Const LvlB As Integer = 5
    Public Const LvlC As Integer = 10
    Public Const LvlD As Integer = 15
    Public Const LvlE As Integer = 20
    Public Const LvlF As Integer = 25
    Public Const LvlG As Integer = 30

    ' Prefix name to add at XX level
    Public Const LvlNameA As String = ""
    Public Const LvlNameB As String = "Aethered"
    Public Const LvlNameC As String = "Lost"
    Public Const LvlNameD As String = "Broken"
    Public Const LvlNameE As String = "Maddened"
    Public Const LvlNameF As String = "Mutated"
    Public Const LvlNameG As String = "Ultra"

    ' Bonus to add at XX level
    Public Const LvlBA As Integer = 0
    Public Const LvlBB As Integer = 5
    Public Const LvlBC As Integer = 10
    Public Const LvlBD As Integer = 15
    Public Const LvlBE As Integer = 20
    Public Const LvlBF As Integer = 25
    Public Const LvlBG As Integer = 30

    Public Sub Init()
        MobDB = New MonDB(MobDBFile)
        RanGen = New Random
    End Sub
End Module
