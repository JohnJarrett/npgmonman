﻿Public Class Form1

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim nMon As MonInst
        Dim l1 As List(Of Integer)
        Dim l2 As List(Of Integer())

        Dim txt As List(Of String)

        Init()

        For i = 0 To MobDB.Count - 1
            nMon = MobDB.GetMon(i)

            l1 = New List(Of Integer)
            l2 = New List(Of Integer())

            txt = New List(Of String)

            For j = 1 To MaxLevel
                nMon.LVL(j)
                l1.Add(nMon.Health())
                l2.Add(nMon.Damage())
            Next

            For k = 0 To MaxLevel - 1
                txt.Add("Level " & GetZeros(k + 1, 1) & " " & nMon.Name(k + 1) & ", H: " & l1(k) & ", Min DMG: " & l2(k)(0) & ", Max DMG: " & l2(k)(1))
            Next

            If Not IO.Directory.Exists(MobTableDir) Then
                IO.Directory.CreateDirectory(MobTableDir)
            End If

            Dim fname As String = MobTableDir & "\" & nMon.Name(1) & "." & MobTableFileExt

            IO.File.WriteAllLines(MobTableDir & "\" & nMon.Name(1) & "." & MobTableFileExt, txt.ToArray)
        Next
    End Sub
End Class
