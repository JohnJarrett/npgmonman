﻿Public Class Mon

    ''' <summary>
    ''' The Monsters base name at level 1
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property Name As String
    ''' <summary>
    ''' The Monsters Base Health at level 1
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property BaseHealth As Integer
    ''' <summary>
    ''' The Monsters base min DMG at level 1
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property BaseMinDam As Integer
    ''' <summary>
    ''' The Monsters base max DMG at level 1
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property BaseMaxDam As Integer

    ''' <summary>
    ''' Makes a monster
    ''' </summary>
    ''' <param name="str">Format should be "[MOB]Name:BaseHP:BaseMinDMG:BaseMaxDMG" E.g. "[MOB]Meow:50:10:15"</param>
    Public Sub New(str As String)
        Name = str.Split(":")(0).Split("]")(1)
        BaseHealth = str.Split(":")(1)
        BaseMinDam = str.Split(":")(2)
        BaseMaxDam = str.Split(":")(3)
    End Sub

    ''' <summary>
    ''' Makes a blank monster, mainly for error handling
    ''' </summary>
    Public Sub New()
        Me.New("[MOB]FAILED:0:0:0")
    End Sub

    ''' <summary>
    ''' Gets the monsters health based off level and optionally its status
    ''' </summary>
    ''' <param name="Lvl">What level is the monster</param>
    ''' <param name="Status">Does it have a status</param>
    ''' <returns></returns>
    Public Function Health(Lvl As Integer, Optional Status As HStat = HStat.Normal) As Integer
        Dim Result As Integer = BaseHealth

        If Lvl > 1 Then
            For i = 1 To Lvl - 1
                Result = Calc(Result, i)
            Next
        End If

        Select Case Status
            Case HStat.Sickly
                Result -= Result * HBonus
            Case HStat.Vibrant
                Result += Result * HBonus
        End Select

        Return Result
    End Function


    ''' <summary>
    ''' Gets the monsters DMG as a 2-dimensional integer array where 0 is the minimum and 1 is the max damage
    ''' </summary>
    ''' <param name="lvl">What level is the monster</param>
    ''' <param name="Status">Does it have a status</param>
    ''' <returns></returns>
    Public Function Damage(lvl As Integer, Optional Status As SStat = SStat.Normal) As Integer()
        Dim Min, Max As Integer
        Dim Result As Integer() = {0, 0}

        Min = BaseMinDam
        Max = BaseMaxDam

        If lvl > 1 Then
            For i = 1 To lvl - 1
                Min = Calc(Min, i)
                Max = Calc(Max, i)
            Next
        End If

        Select Case Status
            Case SStat.Weak
                Min -= Min * SBonus
                Max -= Max * SBonus
            Case SStat.Strong
                Min += Min * SBonus
                Max += Max * SBonus
        End Select

        Result(0) = Min
        Result(1) = Max

        Return Result
    End Function

    ''' <summary>
    ''' Calculates health or damage per level
    ''' </summary>
    ''' <param name="input">What is the last value</param>
    ''' <param name="lvl">what level is it now</param>
    ''' <returns></returns>
    Private Function Calc(input As Integer, lvl As Integer) As Single
        Dim Result, B As Single

        Select Case lvl
            Case <= LvlC
                B = LvlBB
            Case <= LvlD
                B = LvlBC
            Case <= LvlE
                B = LvlBD
            Case <= lvlF
                B = LvlBE
            Case <= LvlG
                B = LvlBF
            Case Else
                B = LvlBG
        End Select

        If Not lvl < 6 Then
            Result = input + ((input) * LBonus) + (input * (B / 100))
        Else
            Result = input + (input * LBonus)
        End If

        Return Result
    End Function
End Class
