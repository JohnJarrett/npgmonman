﻿Public Module Tools

    ''' <summary>
    ''' Gets the prefix name
    ''' </summary>
    ''' <param name="lvl">What level do we need the prefix for</param>
    ''' <returns></returns>
    Public Function GetNamePreByLevel(lvl As Integer) As String
        Dim Result As String

        Select Case lvl
            Case <= LvlB
                Result = LvlNameA
            Case <= LvlC
                Result = LvlNameB
            Case <= LvlD
                Result = LvlNameC
            Case <= LvlE
                Result = LvlNameD
            Case <= LvlF
                Result = LvlNameE
            Case <= LvlG
                Result = LvlNameF
            Case Else
                Result = LvlNameG
        End Select

        Return Result
    End Function

    ''' <summary>
    ''' Better than normal leading zero calculation
    ''' </summary>
    ''' <param name="Input">Original number</param>
    ''' <param name="leadingZ">How many places should there be, E.g. 1 would make anything under 10 have a leading 0, 2 would make anything under 100 have 1 leading 0 and anything under 10 to have 2</param>
    ''' <returns></returns>
    Public Function GetZeros(Input As Integer, leadingZ As Integer) As String
        Dim Result As String = ""

        While (Result.Length + Input.ToString.Length) <= leadingZ
            Result &= "0"
        End While

        Result &= Input

        Return Result
    End Function

End Module
