﻿Public Class MonInst

    ''' <summary>
    ''' The monster that we are instancing
    ''' </summary>
    ''' <returns></returns>
    Private ReadOnly Property MyMon As Mon
    ''' <summary>
    ''' The monsters level
    ''' </summary>
    ''' <returns></returns>
    Private Property MyLevel As Integer = 1
    ''' <summary>
    ''' It's health status
    ''' </summary>
    ''' <returns></returns>
    Private Property MyHStatus As HStat = HStat.Normal
    ''' <summary>
    ''' It's damage status
    ''' </summary>
    ''' <returns></returns>
    Private Property MySStatus As SStat = SStat.Normal

    ''' <summary>
    ''' How much damage has it taken so far
    ''' </summary>
    ''' <returns></returns>
    Private Property DMGTaken As Integer = 0


    ''' <summary>
    ''' Does what it says on the tin, makes a new monster instance
    ''' </summary>
    ''' <param name="nMon">What monster are we instancing</param>
    Public Sub New(nMon As Mon)
        MyMon = nMon
    End Sub

    ''' <summary>
    ''' Gets the monsters full name, e.g. Sickly Mutated Goblin
    ''' </summary>
    ''' <param name="lvl">Optionally get it's name at a level that isn't it's current</param>
    ''' <returns></returns>
    Public Function Name(Optional lvl As Integer = 0) As String
        Dim Result As String = ""
        Dim tlvl As Integer

        If lvl > 0 Then
            tlvl = lvl
        Else
            tlvl = MyLevel
        End If

        If Not MyHStatus = HStat.Read Then
            If Not MyHStatus = HStat.Normal Then
                Result = MyHStatus.ToString & " "
            End If
        End If

        If Not MySStatus = SStat.Read Then
            If Not MySStatus = SStat.Normal Then
                Result = MySStatus.ToString & " "
            End If
        End If

        If tlvl > 5 Then
            Result &= GetNamePreByLevel(tlvl) & " " & MyMon.Name
        Else
            Result &= MyMon.Name
        End If

        Return Result
    End Function

    ''' <summary>
    ''' Gets or sets the monsters health - with any status effect applied
    ''' </summary>
    ''' <param name="DMG">Did it take damage? Well then, how much?</param>
    ''' <returns></returns>
    Public Function Health(Optional DMG As Integer = 0) As Integer
        Dim Result As Integer

        DMGTaken += DMG

        Result = MyMon.Health(MyLevel, MyHStatus) - DMGTaken

        Return Result
    End Function


    ''' <summary>
    ''' Gets the monsters DMG as a 2-dimensional integer array where 0 is the minimum and 1 is the max damage - with any status effect applied
    ''' </summary>
    ''' <returns></returns>
    Public Function Damage() As Integer()
        Dim Result As Integer()

        Result = MyMon.Damage(MyLevel, MySStatus)

        Return Result
    End Function

    ''' <summary>
    ''' Gets or sets the monsters current level
    ''' </summary>
    ''' <param name="nLvl">Optionally set the level</param>
    ''' <returns></returns>
    Public Function LVL(Optional nLvl As Integer = 0) As Integer
        Dim Result As Integer

        If Not nLvl = 0 Then
            MyLevel = nLvl
        End If

        Result = MyLevel

        Return Result
    End Function

    ''' <summary>
    ''' Gets or sets the monsters current status effect for health
    ''' </summary>
    ''' <param name="nStatus">for setting the status</param>
    ''' <returns></returns>
    Public Function HStatus(Optional nStatus As HStat = HStat.Read) As HStat
        Dim Result As HStat

        If Not nStatus = HStat.Read Then
            If nStatus = HStat.Random Then
                Select Case RanGen.Next(RanStsNum)
                    Case < RanStsMin
                        MyHStatus = HStat.Sickly
                    Case > RanStsMax
                        MyHStatus = HStat.Vibrant
                End Select
            Else
                MyHStatus = nStatus
            End If
        End If

        Result = MyHStatus

        Return Result
    End Function

    ''' <summary>
    ''' Gets or sets the monsters current status effect for Damage
    ''' </summary>
    ''' <param name="nStatus">for setting the status</param>
    ''' <returns></returns>
    Public Function SStatus(Optional nStatus As SStat = SStat.Read) As SStat
        Dim Result As SStat

        If Not nStatus = SStat.Read Then
            If nStatus = SStat.Random Then
                Select Case RanGen.Next(RanStsNum)
                    Case < RanStsMin
                        MySStatus = SStat.Weak
                    Case > RanStsMax
                        MySStatus = SStat.Strong
                End Select
            Else
                MySStatus = nStatus
            End If
        End If

        Result = MySStatus

        Return Result
    End Function
End Class
